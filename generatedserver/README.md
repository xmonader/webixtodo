* generate server


$ go-raml server -l python --kind gevent-flask --ramlfile api_specs/main.raml --dir generatedserver
INFO[2018-01-31T08:58:12+02:00] Generating a python server                   
INFO[2018-01-31T08:58:12+02:00] generating file generatedserver/types/client_support.py 
INFO[2018-01-31T08:58:12+02:00] generating file generatedserver/server.py    
INFO[2018-01-31T08:58:12+02:00] generating file generatedserver/types/CreateTodoReqBody.capnp 
INFO[2018-01-31T08:58:12+02:00] generating file generatedserver/types/UpdateTodoReqBody.capnp 
INFO[2018-01-31T08:58:12+02:00] generating file generatedserver/types/Todo.capnp 
INFO[2018-01-31T08:58:12+02:00] generating file generatedserver/types/Todo.py 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/types/UpdateTodoReqBody.py 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/types/CreateTodoReqBody.py 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/handlers/schema/CreateTodoReqBody_schema.json 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/handlers/schema/UpdateTodoReqBody_schema.json 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/handlers/schema/Todo_schema.json 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/handlers/schema/List_Todo_schema.json 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/todos_api.py 
INFO[2018-01-31T08:58:13+02:00] generating file generatedserver/handlers/ListTodosHandler.py 
INFO[2018-01-31T08:58:14+02:00] generating file generatedserver/handlers/CreateTodoHandler.py 
INFO[2018-01-31T08:58:14+02:00] generating file generatedserver/handlers/GetTodoHandler.py 
INFO[2018-01-31T08:58:14+02:00] generating file generatedserver/handlers/UpdateTodoHandler.py 
INFO[2018-01-31T08:58:14+02:00] generating file generatedserver/handlers/__init__.py 
INFO[2018-01-31T08:58:15+02:00] generating file generatedserver/requirements.txt 
INFO[2018-01-31T08:58:15+02:00] generating file generatedserver/index.html   
INFO[2018-01-31T08:58:15+02:00] generating file generatedserver/app.py       
INFO[2018-01-31T08:58:15+02:00] generating file generatedserver/__init__.py  
INFO[2018-01-31T08:58:15+02:00] Generating API Docs to apidocs               
ahmed@ahmedheaven:/home/ahmed/wspace/webixtodo  $ cd generatedserver 
ahmed@ahmedheaven:/home/ahmed/wspace/webixtodo/generatedserver  $ ls
apidocs  app.py  handlers  index.html  __init__.py  requirements.txt  server.py  todos_api.py  types


* runserver
cd generatedserver && python3 server.py

