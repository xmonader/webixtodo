from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine 
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import create_database, database_exists
from enum import Enum
from datetime import datetime, date
import string
import random


Base = declarative_base()

class Todo(Base):
    __tablename__ = "todos"

    id = Column(Integer, primary_key=True)

    title = Column(String(255), nullable=False)
    done = Column(Boolean(), default=False)
    # timestamps
    created_at = Column(
        TIMESTAMP, default=datetime.utcnow, nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow,
                           onupdate=datetime.utcnow, nullable=False)


    def as_dict(self):
        return {"id":self.id, "title":self.title, "done":self.done}

# in memory
dbname = 'sqlite://'
# if not database_exists(dbname):
#     create_database(dbname)
# print("DB created.")


engine = create_engine(dbname, echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine, autoflush=True, autocommit=True)

sess = Session()


### populate some fixtures.
def do_fixtures():

    for i in range(45):
        t = Todo(title="Todo number {}".format(i), done=random.choice([True, False]))
        sess.add(t)

