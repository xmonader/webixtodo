# THIS FILE IS SAFE TO EDIT. It will not be overwritten when rerunning go-raml.

from flask import jsonify, request
from .db import *

def GetTodoHandler(id):
    todo = sess.query(Todo).filter_by(id=id).first()
    if todo:
        return jsonify(todo.as_dict())
    return jsonify("error no todo with id {}".format(id))

