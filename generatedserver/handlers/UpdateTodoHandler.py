# THIS FILE IS SAFE TO EDIT. It will not be overwritten when rerunning go-raml.

from flask import jsonify, request

import json as JSON
import jsonschema
from jsonschema import Draft4Validator
from .db import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
UpdateTodoReqBody_schema = JSON.load(open(dir_path + '/schema/UpdateTodoReqBody_schema.json'))
UpdateTodoReqBody_schema_resolver = jsonschema.RefResolver('file://' + dir_path + '/schema/', UpdateTodoReqBody_schema)
UpdateTodoReqBody_schema_validator = Draft4Validator(
    UpdateTodoReqBody_schema, resolver=UpdateTodoReqBody_schema_resolver)


def UpdateTodoHandler(id):

    inputs = request.get_json()

    try:
        UpdateTodoReqBody_schema_validator.validate(inputs)
    except jsonschema.ValidationError as e:
        return jsonify(errors="bad request body"), 400
    todo = sess.query(Todo).filter_by(id=id).first()
    if todo:
        if 'title' in inputs:
            todo.title = inputs['title']
        if 'done' in inputs:
            todo.done = inputs['done']
        
        sess.add(todo)
        # sess.commit()
    return jsonify(todo.as_dict())
