# THIS FILE IS SAFE TO EDIT. It will not be overwritten when rerunning go-raml.

from flask import jsonify, request

import json as JSON
import jsonschema
from jsonschema import Draft4Validator
import os
from .db import *




dir_path = os.path.dirname(os.path.realpath(__file__))
CreateTodoReqBody_schema = JSON.load(open(dir_path + '/schema/CreateTodoReqBody_schema.json'))
CreateTodoReqBody_schema_resolver = jsonschema.RefResolver('file://' + dir_path + '/schema/', CreateTodoReqBody_schema)
CreateTodoReqBody_schema_validator = Draft4Validator(
    CreateTodoReqBody_schema, resolver=CreateTodoReqBody_schema_resolver)


def CreateTodoHandler():

    inputs = request.get_json()
    print(inputs)

    try:
        CreateTodoReqBody_schema_validator.validate(inputs)
    except jsonschema.ValidationError as e:
        return jsonify(errors="bad request body"), 400
    title = inputs['title']
    done = inputs.get('done', False)
    todo = Todo(title=title, done=done)
    sess.add(todo)
    # sess.commit()
    return jsonify(todo.as_dict())
