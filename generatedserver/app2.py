from app import app
from flask import Flask, request, send_from_directory, send_file, render_template, jsonify
from handlers.db import *

def register_more():
    @app.route("/sayhi")
    def sayhi():
        return "heloooo"

    @app.route('/static/<path:path>')
    def send_public(path):
        return send_from_directory('static' + '/' + path)


    @app.route('/dispatchtodos', methods=["POST"])
    def dispatch_todos():
        posted_data = dict(request.form)
        print("posted__data:", posted_data)
        todoid = posted_data.get('id', None)
        if todoid:
            todoid = todoid[0]
        webix_op = posted_data.pop('webix_operation')[0]
        # import ipdb; ipdb.set_trace()
        if todoid:
            todo = sess.query(Todo).filter_by(id=todoid).first()
            if todo:
                if webix_op == "update":
                    print("UPDATING....")
                    for k,v in posted_data.items():
                        cleanv = v[0]
                        if cleanv in ["false", "true"]:
                            cleanv = cleanv=="true"
                        
                        setattr(todo, k, cleanv)
                    sess.add(todo)
                elif webix_op == "delete":
                    print("DELETING....")
                    sess.delete(todo)
        if webix_op == "insert":
            print("CREATING>....")
            print(posted_data)

            title = posted_data.get('title', None)
            title = title[0] if title else None
            done = posted_data['done'][0] == True
            todo = Todo(title=title, done=done)
            sess.add(todo)
                
        return jsonify( {"status":"ok"})


        
    @app.route('/listtodos')
    def list_todos():
        return render_template('listtodos.html')


    @app.route('/viewtodo')
    def view_todo(id):
        return render_template('detailstodo.html')


    @app.route('/edittodo')
    def update_todo(id):
        return render_template('updatetodo.html')

    
    @app.route('/createtodo')
    def create_todo():
        return render_template('createtodo.html')